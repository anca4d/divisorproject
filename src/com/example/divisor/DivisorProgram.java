package com.example.divisor;

import java.util.Scanner;

public class DivisorProgram {

	public static void main(String[] args) {

		// Declare a counter of the divisors for number 'a'
		// initialize the int 'divisorCounterA'
		int divisorCounterA = 0;
		
		// Declare a counter of the divisors for number 'b'
		// initialize the int 'divisorCounterB'
		int divisorCounterB = 0;
		
		// Declare a Scanner for getting the values from the keyboard
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter numer a:");
		
		// Declare and get the value of integer 'a' from the keyboard
		int a = scan.nextInt();
		
		System.out.println("Please enter numer b:");
		// Declare and get the value of integer 'b' from the keyboard
		int b = scan.nextInt();
		
		if(a == 0 || b == 0) {
			System.out.println("Numerele 'a' si 'b' trebuie sa fie > 0!!");
			return;
		}

		// for loop for veryfying and counting the divisors of number 'a'
		for (int i = 1; i <= a; i++) {
			if (a % i == 0)
				divisorCounterA++;
		}

		// for loop for veryfying and counting the divisors of number 'b'
		for (int j = 1; j <= b; j++) {
			if (b % j == 0)
				divisorCounterB++;
		}

		// test if the numbers have the same number of divisors and print the result
		if (divisorCounterA == divisorCounterB) {
			System.out.println("Number a = " + a + " and number b = " + b + " have the same number of divisors = "
					+ divisorCounterA);
		} else {
			System.out.println("Number a = " + a + " and number b = " + b + " don't have the same number of divisors.\n"
					+ "Divisors of a = " + divisorCounterA + "\nDivisors of b = " + divisorCounterB);
		}
	}
}